var express = require('express');
var app = express();
const phantomRunner = require('./phantom');


const MyEmitter = require('events');
const emitter = new MyEmitter();
emitter.on('check-again', (a, b) => {

      const url = "https://www.youtube.com/";
      const filename = "images/bawa.jpeg";
      const options = { 
        output : { format: 'jpeg', quality: '100' },
        viewport : { width: 2560, height: 1440 }
      };

      console.log('.... initiating rendering');      
      phantomRunner(url,filename, options).
        then((data)=> {
          console.log('.... rendering success');
          setTimeout(() => {
            emitter.emit('check-again');
           }, 5000);
        })
        .catch((err)=>{ 
          setTimeout(() => {
            emitter.emit('check-again');
           }, 5000);
        })
});

app.get('/', function(req, res){
  res.json({ hello: 'there' })
});

app.listen(3000,()=>{
  emitter.emit('check-again');
});