const phantom = require("phantom");
let _ph, _page, _outObj;

module.exports = (url,filename, options) => (
    new Promise((resolve, reject)=>{
        phantom.create().then(ph => {
            _ph = ph;
            return _ph.createPage();
        }).then((page) => {
            _page = page;
            return _page.open(url);
        }).then((status) => {
            console.log(status);
            return _page.property('content')
        }).then((content) => {
           
          setTimeout(()=>{
                _page.property('viewportSize', options.viewport)
                _page.render(filename, options.output);
                _page.close();
                _ph.exit();
                console.log('success rendering...');
          },5000);
          resolve({ status: 'success', image: 'http://localhost:3000/'+filename }); 

        }).catch(e => reject(e));
    }));

